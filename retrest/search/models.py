from django.db import models

# Create your models here.


class Places(models.Model):
    name = models.CharField(max_length=200)
    short_description = models.TextField(max_length=200)
    description = models.TextField()
    country = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    street = models.CharField(max_length=200)
    building = models.CharField(max_length=100)
    average_price = models.IntegerField(null=False)
    nation_food = models.CharField(max_length=200)
    phone = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='search/')
    rating = models.FloatField(null=False)
