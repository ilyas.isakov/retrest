from django.shortcuts import render
from .models import Places
from django.db.models import Q
from django.core.paginator import Paginator
# Create your views here.


def search(request):
    query_dict = request.GET
    query = query_dict.get("q")
    obj_list = None
    if query is not None:
        obj_list = Places.objects.filter(
            Q(name__icontains=query) | Q(short_description__icontains=query) | Q(city__icontains=query) |
            Q(nation_food__icontains=query)
        )
    paginator = Paginator(obj_list, 1)
    page = request.GET.get('page')
    page_list = paginator.get_page(page)
    context = {
        "object_list": obj_list,
        "list": page_list,
        "query": query,
    }
    return render(request, 'search/search.html', context=context)


def place_view(request, id=None):
    place_obj = None
    if id is not None:
        place_obj = Places.objects.get(id=id)
    context = {
        "object": place_obj,
        "id": id,
    }
    return render(request, 'places/place.html', context=context)


def top_view(request):
    obj_list = reversed(Places.objects.order_by('rating')[:100])
    context = {
        "object_list": obj_list,
    }
    return render(request, 'search/top.html', context=context)
