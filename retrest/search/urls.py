from django.urls import path
from . import views

urlpatterns = [
    path('places/', views.search, name='search'),
    path('places/<int:id>', views.place_view),
    path('places/top', views.top_view, name='top')
]
